package com.example.mamagulashviliscalculator

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageButton
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_calculator.*
import kotlinx.android.synthetic.main.activity_calculator.view.*
import kotlin.math.sqrt

class CalculatorActivity : AppCompatActivity(), View.OnClickListener, View.OnLongClickListener {
    private var firstVariable = 0.0
    private var secondVariable = 0.0
    private var operation = ""
    private var clickCount = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calculator)
        init()
    }

    private fun init() {
        oneBT.setOnClickListener(this)
        twoBT.setOnClickListener(this)
        threeBT.setOnClickListener(this)
        fourBT.setOnClickListener(this)
        fiveBT.setOnClickListener(this)
        sixBT.setOnClickListener(this)
        sevenBT.setOnClickListener(this)
        eightBT.setOnClickListener(this)
        nineBT.setOnClickListener(this)
        backBT.setOnLongClickListener(this)


    }
    private fun operation() {
        var result = 0.0
        when (operation) {
            ":" -> {
                result = firstVariable / secondVariable
            }
            "x" -> {
                result = firstVariable * secondVariable
            }
            "-" -> {
                result = firstVariable - secondVariable
            }
            "+" -> {
                result = firstVariable + secondVariable
            }
            "%" -> {
                result = firstVariable * secondVariable / 100
            }
            "!" -> {
                result = 1.0
                while (1<=firstVariable){
                    result *= firstVariable
                    firstVariable--
                }
                resultTV.append(result.toString())
            }

        }
        resultTV.text = result.toString()
    }

    fun zero(view: View) {
        operationTV.append("0")
        if (operation == ":" ) {
            Toast.makeText(this, "Divide By Zero ERROR", Toast.LENGTH_SHORT).show()
            operationTV.text = ""
            resultTV.text = ""
        }
    }

    fun dot(view: View) {

        if (operationTV.text.toString().isEmpty()){
            operationTV.append("0.")
        }else{
            operationTV.append(("."))
        }
        dotFun()
    }

    private fun dotFun() {
        clickCount++
        if (operationTV.text.toString().isNotEmpty()){
            if (clickCount >= 1) {
                dotBT.isEnabled = false

            }
        }
    }

    fun equal(view: View) {
        val value = operationTV.text.toString()

        if (value.isNotEmpty()) {
            secondVariable = value.toDouble()
            operation()
        }
    }
    fun factorial(view: View){
        val value = operationTV.text.toString()
        if (value.isNotEmpty()) {
            firstVariable = operationTV.text.toString().toDouble()
            operation = "!"
            resultTV.append("!")


        }
    }
    fun percent(view: View){
        val value = operationTV.text.toString()
        if (value.isNotEmpty()){
            firstVariable = operationTV.text.toString().toDouble()
            operation = "%"
            operationTV.text = ""
            resultTV.append("%")
        }
    }
    fun plus(view: View) {

        val value = operationTV.text.toString()
        if (value.isNotEmpty()) {
            firstVariable = operationTV.text.toString().toDouble()
            operation = "+"
            operationTV.text = ""
            resultTV.append("+")
        }
    }

    fun minus(view: View) {
        val value = operationTV.text.toString()
        if (value.isNotEmpty()) {
            firstVariable = operationTV.text.toString().toDouble()
            operation = "-"
            operationTV.text = ""
            resultTV.append("-")
        }
    }

    fun multiplication(view: View) {
        val value = operationTV.text.toString()
        if (value.isNotEmpty()) {
            firstVariable = operationTV.text.toString().toDouble()
            operation = "x"
            operationTV.text = ""
            resultTV.append("*")
        }

    }

    fun divide(view: View) {
        val value = operationTV.text.toString()
        if (value.isNotEmpty()) {
            firstVariable = operationTV.text.toString().toDouble()
            operation = ":"
            resultTV.append("/")
            operationTV.text = ""

        }
    }

    fun delete(view: View) {
        val value1 = resultTV.text.toString()
        val value2 = operationTV.text.toString()
        if (value1.isNotEmpty() || value2.isNotEmpty()) {
            resultTV.text = ""
            operationTV.text = ""
        }
        dotBT.isEnabled = true

    }

    fun backward(view: View) {
        val value = operationTV.text.toString()
        if (value.isNotEmpty()) {
            operationTV.text = value.substring(0, value.length - 1)
        }
        dotBT.isEnabled = true
    }

    override fun onClick(v: View?) {
        val button: Button = v as Button
        operationTV.text = operationTV.text.toString() + button.text.toString()
    }


    override fun onLongClick(v: View?): Boolean {
        operationTV.text = ""
        resultTV.text = ""
        return true
    }

}